--Binar to HEXA...... SW0=1, SW1=1
--Binar to BCD+3..... SW0=0, SW1=1
--Binar to GRAY...... SW0=1, SW1=0
--Binar to BCD........SW0=0, SW1=0

--For most of calculations we used http://www.32x8.com , what is an easier way to calculate maps.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;  

-- Input/output description
entity top is
    port (
        SW_EXP: in std_logic_vector(15 downto 0);	                                                          --expanded switches
        LED_EXP: out std_logic_vector(15 downto 0); 		 						  --expanded leds
		  BTN0: in std_logic;     									  -- button 0
		  BTN1: in std_logic;    									  -- button 1
		  SW0: in std_logic;    									  -- switch 0
		  SW1: in std_logic;     									  -- switch 1
		  CLK: in std_logic;
		  LED: out std_logic_vector(3 downto 0);		      			  		 --led
		  D_POS: out std_logic_vector(3 downto 0);              					 --display positions
		  D_SEG: out std_logic_vector(6 downto 0) 
    );
end top;

-- Internal structure description
architecture Behavioral of top is

signal clk_10: std_logic := '0';									  	--clock parameter
signal tmp_10: std_logic_vector(11 downto 0) := x"000";                                                         --hexadecimal value

begin

process (CLK)												       --fast clock function
    begin
        if rising_edge(CLK) then
            tmp_10 <= tmp_10 + 1;
            if tmp_10 = x"032" then
                tmp_10 <= x"000";
                clk_10 <= not clk_10;
            end if;
        end if;
    end process; 

process (clk_10)
 begin
	if SW0 = '0' and SW1 ='0' then 						       			     --BCD condition (SW0 = '0' and SW1 ='0')
		if clk_10 = '0' then								             --clock as a fast D_POS switcher
			D_POS(3 downto 0) <= "1110";						             --1. positon for units(BCD)
--G
			D_SEG(6) <= ((not SW_EXP(3)) and (not SW_EXP(2)) and (not SW_EXP(1)) and (not SW_EXP(0))) or
	                  	    (SW_EXP(3) and SW_EXP(4) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
				    (SW_EXP(3) and (not SW_EXP(2)) and SW_EXP(1) and (not SW_EXP(0))) or
				    (SW_EXP(3) and SW_EXP(4) and SW_EXP(1) and SW_EXP(0)) or
				    (SW_EXP(3) and SW_EXP(2) and SW_EXP(1) and SW_EXP(0)) or
				    ((not SW_EXP(3)) and SW_EXP(2) and (not SW_EXP(1)) and SW_EXP(0)) or
				    ((not SW_EXP(3)) and SW_EXP(4) and (not SW_EXP(1)) and SW_EXP(0));
--F
			D_SEG(5) <=  ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(4)) or
				     ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3)) or
			             ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(3) and SW_EXP(4)) or
				     (SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
				     ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3))) or
				     (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(3)) and SW_EXP(4)) or
			             (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3)) or
			             (SW_EXP(0) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4));
--E
			D_SEG(4) <=  (SW_EXP(4)) or
			             ((not SW_EXP(3)) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
			             (SW_EXP(3) and SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
			             ((not SW_EXP(3)) and (not SW_EXP(2)) and SW_EXP(1) and SW_EXP(0));
--D
			D_SEG(3) <=  ((not SW_EXP(3)) and SW_EXP(4) and (not SW_EXP(2)) and (not SW_EXP(1))) or
			             ((not SW_EXP(3)) and (not SW_EXP(4)) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
			             (SW_EXP(3) and SW_EXP(4) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
				     (SW_EXP(3) and ( not SW_EXP(4)) and SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				     (SW_EXP(3) and SW_EXP(4) and (not SW_EXP(2)) and SW_EXP(1)) or
				     (SW_EXP(3) and SW_EXP(4) and SW_EXP(1) and SW_EXP(0)) or
				     ((not SW_EXP(3)) and (not SW_EXP(4)) and (not SW_EXP(2)) and SW_EXP(1) and SW_EXP(0)) or
				     ((not SW_EXP(3)) and SW_EXP(4) and (not SW_EXP(1)) and SW_EXP(0));
--C 
			D_SEG(2) <= (SW_EXP(3) and (not SW_EXP(4)) and (not SW_EXP(2)) and (not SW_EXP(1)) and (not SW_EXP(0))) or
				    ((not SW_EXP(3)) and (not SW_EXP(4)) and SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				    (SW_EXP(3) and (not SW_EXP(4)) and SW_EXP(2) and (not SW_EXP(1)) and SW_EXP(0));
--B
			D_SEG(1) <= ((not SW_EXP(3)) and SW_EXP(4) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
				    (SW_EXP(3) and (not SW_EXP(4)) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
				    (SW_EXP(3) and SW_EXP(4) and SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				    ((not SW_EXP(3)) and SW_EXP(4) and (not SW_EXP(2)) and SW_EXP(1) and SW_EXP(0)) or
				    (SW_EXP(3) and (not SW_EXP(4)) and (not SW_EXP(2)) and SW_EXP(1) and SW_EXP(0)) or
				    ((not SW_EXP(3)) and (not SW_EXP(4)) and (not SW_EXP(2)) and (not SW_EXP(1)) and SW_EXP(0));
--A
			D_SEG(0) <= ((not SW_EXP(3)) and SW_EXP(4) and (not SW_EXP(2)) and (not SW_EXP(1)) and (not SW_EXP(0))) or
                       		    ((not SW_EXP(3)) and (not SW_EXP(4)) and SW_EXP(2) and (not SW_EXP(1)) and (not SW_EXP(0))) or
	                            (SW_EXP(3) and SW_EXP(4) and (not SW_EXP(2)) and SW_EXP(1) and (not SW_EXP(0))) or
			            (SW_EXP(3) and (not SW_EXP(4)) and SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or  
                                    ((not SW_EXP(3)) and (not SW_EXP(4)) and (not SW_EXP(2)) and SW_EXP(1) and SW_EXP(0)) or
				    (SW_EXP(3) and SW_EXP(4) and SW_EXP(2) and SW_EXP(1) and SW_EXP(0)) or
				    ((not SW_EXP(3)) and SW_EXP(4) and SW_EXP(2) and (not SW_EXP(1)) and SW_EXP(0));

		else
                        D_POS(3 downto 0) <= "1101";				   --2. positon for tens(BCD)
--G 
			D_SEG(6) <= (not SW_EXP(0)) or 
				    ((not SW_EXP(2)) and (not SW_EXP(1)));
--F
			D_SEG(5) <= (SW_EXP(2) and (not SW_EXP(0)) and SW_EXP(1)) or
				    (SW_EXP(0))or
			            (SW_EXP(1) and SW_EXP(3));
--E
			D_SEG(4) <= (SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				    (SW_EXP(3) and SW_EXP(1) and (not SW_EXP(0))) or
			            (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3)) or
			            (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)));
--D
			D_SEG(3) <= (SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				    (SW_EXP(3) and SW_EXP(1) and (not SW_EXP(0))) or
			            (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)));
--C
			D_SEG(2) <= (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(3))) or
			            (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2));
--B
			D_SEG(1) <= '0';
--A
			D_SEG(0) <= (SW_EXP(2) and SW_EXP(1) and (not SW_EXP(0))) or
				    (SW_EXP(3) and SW_EXP(1) and (not SW_EXP(0))) or
			            (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)));
        
		end if;
		
	elsif SW0 = '1' and SW1 ='1' then 				        --HEX condition (SW0 = '1' and SW1 ='1') 
		if clk_10 = '1' then
       		        D_POS(3 downto 0) <= "1110";				--1. positon for units(HEX)
--G
			D_SEG(6) <= ((not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    ((not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
			            (SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4)));
--F
			D_SEG(5) <= ((not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(4)) or
				    ((not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3)) or
	                            ((not SW_EXP(1)) and SW_EXP(3) and SW_EXP(4)) or
                                    (SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4)); 
--E
			D_SEG(4) <= ((not SW_EXP(1)) and SW_EXP(4)) or
				    ((not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
                                    ((not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)));
--D
			D_SEG(3) <= (SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
	                            ((not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
	                            ((not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
                                    (SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4)));
--C
			D_SEG(2) <= (SW_EXP(1) and SW_EXP(2) and (not SW_EXP(4))) or
	                            (SW_EXP(1) and SW_EXP(2) and SW_EXP(3)) or
                                    ((not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4)));
--B
			D_SEG(1) <= (SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
	                            (SW_EXP(1) and SW_EXP(3) and SW_EXP(4)) or
	                            (SW_EXP(1) and SW_EXP(2) and (not SW_EXP(4))) or
 	                            ((not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4));
--A
			D_SEG(0) <= ((not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
	                            ((not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
	                            (SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
	                            (SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4));
		else
                        D_POS(3 downto 0) <= "1101";				--2. positon for tens(HEX)
--G
			D_SEG(6) <= '1';
--F
			D_SEG(5) <= SW_EXP(0);
--E
			D_SEG(4) <= SW_EXP(0);
--D
			D_SEG(3) <= SW_EXP(0);
--C
			D_SEG(2) <= '0';
--B
			D_SEG(1) <= '0';
--A
			D_SEG(0) <= SW_EXP(0);
		end if;
		
	elsif SW0 = '0' and SW1 ='1' then 				   --BCD+3 condition (SW0 = '0' and SW1 ='1')  
		if clk_10 = '1' then
       		        D_POS(3 downto 0) <= "1110";                       --1. positon for units(BCD+3)
--G
			D_SEG(6) <= (SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(3)) and (not SW_EXP(4))) or
			            ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
                                    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
			       	    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
			   	    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(4)) and SW_EXP(3)) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4));
--F
			D_SEG(5) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(4))) or
				    (SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)));
--E
			D_SEG(4) <= (not SW_EXP(4)) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3));
--D
			D_SEG(3) <= (SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4));
--C
			D_SEG(2) <= ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4));
--B
			D_SEG(1) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3)) or
				    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3));
--A
			D_SEG(0) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
		 		    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4));
		 
		else
		 
		 	D_POS(3 downto 0) <= "1101";                       --2. positon for tens(BCD+3)
--G
			D_SEG(6) <= (not SW_EXP(0)) or
				    ((not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4)));
--F
			D_SEG(5) <= (SW_EXP(1)) or
				    (SW_EXP(0)) or
				    (SW_EXP(2) and SW_EXP(3) and SW_EXP(4));
--E
			D_SEG(4) <= ((not SW_EXP(0)) and SW_EXP(1)) or
				    (SW_EXP(1) and SW_EXP(2)) or
				    (SW_EXP(1) and SW_EXP(3) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4)));
--D
			D_SEG(3) <= ((not SW_EXP(0)) and SW_EXP(1)) or
				    ((not SW_EXP(0)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4)));
--C
			D_SEG(2) <= (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2)) or
				    (SW_EXP(0) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)));
--B
			D_SEG(1) <= '0';
--A
			D_SEG(0) <= ((not SW_EXP(0)) and SW_EXP(1)) or
				    ((not SW_EXP(0)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4)));
		 end if;

	elsif SW0 = '1' and SW1 ='0' then 				   --Gray condition (SW0 = '1' and SW1 ='0')  
		if clk_10 = '1' then
                        D_POS(3 downto 0) <= "1110";                       --1. positon for units(Gray)
--G 
			D_SEG(6) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4))) or
				    ((not SW_EXP(1)) and  SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and  SW_EXP(3) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(3) and (not SW_EXP(3)) and SW_EXP(4)) ;

--F		
			D_SEG(5) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(4)) or 
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or 
				    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or 
				    ((not SW_EXP(1)) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or 
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3)) or 
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and SW_EXP(4));

--E
			D_SEG(4) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(3)) and SW_EXP(4)) or
				    (SW_EXP(3) and (not SW_EXP(4)));
--D
			D_SEG(3) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or 
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or 
			 	    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(4))) or
				    ((not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and  SW_EXP(1) and (not SW_EXP(3)) and SW_EXP(4));
--C
			D_SEG(2) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or 
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4));
--B
			D_SEG(1) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and SW_EXP(4)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4)));
--A
			D_SEG(0) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2)) and SW_EXP(3) and SW_EXP(4)) or
				    ((not SW_EXP(0)) and SW_EXP(1) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3)) and (not SW_EXP(4))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3) and (not SW_EXP(4))) or
				    (SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3)) and SW_EXP(4)); 

		else	
	  
		        D_POS(3 downto 0) <= "1101";                        --2. positon for tens(Gray)	
--G
			D_SEG(6) <= ((not SW_EXP(0)) and (not SW_EXP(1)) and SW_EXP(2)) or 
				    ((not SW_EXP(0)) and (not SW_EXP(2))) or
				    ((SW_EXP(1)) and SW_EXP(2));
--F
			D_SEG(5) <= (SW_EXP(0) and (not SW_EXP(1)) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(3)) or 
				    (SW_EXP(1) and (not SW_EXP(2)) and (not SW_EXP(3))) or
				    (SW_EXP(0) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(1) and SW_EXP(2));
--E
			D_SEG(4) <= ((not SW_EXP(0)) and SW_EXP(1) and (not SW_EXP(2))) or
				    (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and SW_EXP(3)) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(1) and SW_EXP(2) and SW_EXP(3));
--D
			D_SEG(3) <= (not SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2))) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3))) or 
				    (SW_EXP(1) and SW_EXP(2) and SW_EXP(3));
--C
			D_SEG(2) <= (SW_EXP(0) and (not SW_EXP(1)) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(0) and (not SW_EXP(2)));
--B
			D_SEG(1) <= '0';
--A
			D_SEG(0) <= (not SW_EXP(0) and SW_EXP(1) and (not SW_EXP(2))) or
				    (SW_EXP(0) and SW_EXP(1) and SW_EXP(2) and (not SW_EXP(3))) or
				    (SW_EXP(1) and SW_EXP(2) and SW_EXP(3));
		end if;	
	end if;
end process;	 
LED_EXP <= SW_EXP;
end Behavioral;
